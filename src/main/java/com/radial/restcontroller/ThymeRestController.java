package com.radial.restcontroller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ThymeRestController {

	
	@PostMapping("/processRequest")
	void response(@RequestParam("fname") String fname, @RequestParam("lname") String lname)
	{
		System.out.println("First Name " + fname);
		
		System.out.println("Last Name " + lname);
	}
}
