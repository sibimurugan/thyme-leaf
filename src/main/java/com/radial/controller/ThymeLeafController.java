package com.radial.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ThymeLeafController {
	
	@GetMapping(path = { "/home", "/" })
	String getHome() {
		return "home";
	}
	
	
	@GetMapping("/viewTransLog")
	String transLog() {
		return "trans";
	}

}
